@extends('serviceForm')
@section('formTitle', 'Editar serviço')
@section('route_name', route('post.editService', $service->id))
@section('btnLabel', 'Atualizar')