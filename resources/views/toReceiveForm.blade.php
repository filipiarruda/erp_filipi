@extends('dashboard')

@section('title', 'Contas a Receber')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('formTitle')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                	<form method="POST" action="@yield('route_name')">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="{{ isset($receive->name) ? $receive->name : '' }}">
                        {{ csrf_field() }}
                    	</div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Valor</label>
                        <div class="input-group">
                    <div class="input-group-addon">R$</div>
                    <input type="text" class="form-control money" id="value" name="value" placeholder="0.000,00" value="{{isset($receive->value) ? number_format($receive->value, 2, ',', '.') : ''}}">
                  </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Tipo de pagamento</label>
                        <select name="payment_type" id="payment_type" class="form-control">
                          <option value="1" {{isset($receive->payment_type) && $receive->payment_type == '1' ? 'selected' : ''}}>Dinheiro</option>
                          <option value="2" {{isset($receive->payment_type) && $receive->payment_type == '2' ? 'selected' : ''}}>Cheque</option>
                          <option value="3" {{isset($receive->payment_type) && $receive->payment_type == '3' ? 'selected' : ''}}>Depósito em conta</option>
                          <option value="4" {{isset($receive->payment_type) && $receive->payment_type == '4' ? 'selected' : ''}}>Cartão</option>
                          <option value="5" {{isset($receive->payment_type) && $receive->payment_type == '5' ? 'selected' : ''}}>Boleto</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Data de Vencimento</label>
                        <input type="date" class="form-control" name="due_date" id="due_date" value="{{isset($receive->due_date) ? $receive->due_date : ''}}">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Data de Recebimento</label>
                        <input type="date" name="received_date" class="form-control" id="received_date" value="{{isset($receive->received_date) ? $receive->received_date : ''}}">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Empresa</label>
                        <select name="company_id" id="company_id" class="form-control">
                          <option value="">Selecione</option>
                          @foreach($companies as $company)
                          <option value=" {{$company->id}}" {{isset($receive->company_id) && $receive->company_id == $company->id ? 'selected' : ''}}>{{$company->name}}</option>
                          @endforeach;
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Cliente</label>
                        <select name="customer_id" id="customer_id" class="form-control">
                          <option value="">Selecione</option>
                          @foreach($customers as $customer)
                            <option value="{{$customer->id}}" {{isset($receive->customer_id) && $receive->customer_id == $customer->id ? 'selected' : ''}}>{{$customer->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success">@yield('btnLabel')</button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
@endsection