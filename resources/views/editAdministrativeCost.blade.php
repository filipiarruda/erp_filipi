@extends('administrativeCostsForm')
@section('formTitle', 'Editar despesa')
@section('route_name', route('post.editCost', $cost->id))
@section('btnLabel', 'Atualizar')