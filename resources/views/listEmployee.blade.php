@extends('dashboard')

@section('title', 'Lista de funcionários')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Funcionários</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Login</th>
                                <th>E-mail</th>
                                <th>Ativo</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		@foreach($list as $employee)
                                @if($employee->resignation_date <> '0001-01-01')
                                <tr class="danger">
                                @else
                                <tr>
                                @endif
									<td>{{ $employee->id }}</td>
									<td>{{ $employee->name }}</td>
									<td>{{ $employee->login }}</td>
									<td>{{ $employee->email }}</td>
                                    <td>
                                        @if($employee->resignation_date <> '0001-01-01')
                                        Desligado da Empresa
                                        @else
                                        Ativo
                                        @endif
                                    </td>
									<td class="text-center"><a href="{{route('form.editEmployee', $employee->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
@endsection