@extends('dashboard')

@section('title', 'Departamento')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('formTitle')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                	<form method="POST" action="@yield('route_name')">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="{{ isset($department->name) ? $department->name : '' }}">
                        {{ csrf_field() }}
                    	</div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Ativo</label>
                    		<select name="active" id="active" class="form-control">
                          @if(isset($department->active) && $department->active == '1')
                          <option value="1" selected>Sim</option>
                          <option value="0">Não</option>
                          @elseif(isset($department->active) && $department->active == '0')
                          <option value="1">Sim</option>
                          <option value="0" selected>Não</option>
                          @else
                          <option value="1">Sim</option>
                          <option value="0" selected>Não</option>
                          @endif
                        </select>
                    	</div>
                    </div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success">@yield('btnLabel')</button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
@endsection