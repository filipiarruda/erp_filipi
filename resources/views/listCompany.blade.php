@extends('dashboard')

@section('title', 'Empresas do grupo')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de Empresas do Grupo</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Ativo</th>
                        	</thead>
                        	<tbody>
                        		@foreach($list as $company)
								<tr>
									<td>{{ $company->id }}</td>
									<td>{{ $company->name }}</td>
									@if($company->active == 'true' || $company->active == 1)
									<td>Sim</td>
									@endif
									@if($company->active == 'false' || $company->active == 0)
									<td>Não</td>
									@endif
								</tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
@endsection