@extends('dashboard')
@section('title', 'Contrato')
@section('content')
<div class="row">
	<div class="col-md-12">
		<h1 class="page-header">@yield('formTitle')</h1>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form action="@yield('route_name')" method="post">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Contratante</label>
						<select name="customer_id" id="customer_id" class="form-control">
							<option value="">Selecione</option>
							@foreach($customers as $customer)
							<option value="{{$customer->id}}">{{$customer->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Vendedor</label>
						<select name="salesman" id="salesman" class="form-control">
							<option value="">Selecione</option>
							@foreach($employees as $employee)
							<option value="{{$employee->id}}">{{$employee->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Data de início</label>
						<input type="date" name="date_contract" id="date_contract" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Data de fim</label>
						<input type="date" name="end_date" id="end_date" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Prazo(dias)</label>
						<input type="text" name="deadline" id="deadline" class="form-control">
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Serviços no contrato
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Ações
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a data-toggle="modal" data-target="#myModal" style="cursor: pointer;">Adicionar serviço</a>
                                        </li>
                                        <li><a onclick="cleanServices();">Limpar serviços do contrato</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="table-responsive">
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Serviço</th>
                                            <th>Valor</th>
                                            <th><i class="fa fa-edit"></i></th>
                                            <th><i class="fa fa-trash"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody id="services-content">
                                        <tr>
                                            <td>Serviço teste</td>
                                            <td>R$ {{number_format('2500.00', 2, ',', '.')}}</td>
	                                        <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                            <td><a href="#"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group">
                    		<label for="">Valor</label>
                    		<input type="text" class="form-control money" name="value" id="value">
                    	</div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group">
                    		<label for="">Desconto</label>
                    		<input type="text" class="form-control money" name="descount" id="descount">
                    	</div>
                    </div>

                    <div class="col-md-3">
                    	<div class="form-group">
                    		<label for="">Entrada</label>
                    		<input type="text" class="form-control money" name="enterance" id="entrance"></div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group">
                    		<label for="">Parcelas</label>
                    		<input type="number" class="form-control" name="installment" id="installment"></div>
                    </div>
                    <div class="col-md-2">
                    	<div class="form-group">
                    		<label for="">Vencimento</label>
                    		<input type="text" class="form-control" name="maturity" id="maturity"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                	<div class="col-lg-2 col-lg-offset-10"><button class="btn btn-success pull-right">@yield('btnLabel')</button></div>
                    </div>
                </div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Serviços</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-stripped" id="dataTable">
                <thead>
                    <tr>
                        <th>Serviço</th> 
                        <th>Valor</th>
                        <th>Add</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($services as $service)
                    <tr>
                        <td>{{$service->name}}</td>
                        <td>R$ {{number_format($service->value, 2, ',', '.')}}</td>
                        <td><a onclick="addService('{{$service->id}}', '{{$service->name}}', 'R$ {{number_format($service->value, 2, ',', '.')}}', '{{$service->value}}');">Add</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Ok</button>
      </div>
    </div>
  </div>
</div>
@endsection