@extends('dashboard')

@section('title', 'Serviços')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('formTitle')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                	<form method="POST" action="@yield('route_name')">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="{{ isset($service->name) ? $service->name : '' }}">
                        {{ csrf_field() }}
                    	</div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Ativo</label>
                    		<select name="active" id="active" class="form-control">
                          @if(isset($service->active) && $service->active == '1')
                          <option value="1" selected>Sim</option>
                          <option value="0">Não</option>
                          @elseif(isset($service->active) && $service->active == '0')
                          <option value="1">Sim</option>
                          <option value="0" selected>Não</option>
                          @else
                          <option value="1">Sim</option>
                          <option value="0">Não</option>
                          @endif
                        </select>
                    	</div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Departamento</label>
                        <select name="department_id" id="department_id" class="form-control">
                          @foreach($departments as $department)
                          <option value="{{$department->id}}" {{isset($service->department_id) && $service->department_id == $department->id ? 'selected' : ''}}>{{$department->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Empresa que realiza o serviço</label>
                        <select name="company_id" id="company_id" class="form-control">
                          @foreach($companies as $company)
                          <option value="{{$company->id}}" {{isset($service->company_id) && $service->company_id == $company->id ? 'selected' : ''}}>{{$company->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Valor</label>
                        <div class="input-group">
                          <div class="input-group-addon">R$</div>
                          <input type="text" class="form-control money" id="value" name="value" placeholder="0.000,00" value="{{isset($service->value) ? number_format($service->value, 2, ',', '.') : ''}}">
                  </div>
                      </div>
                    </div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success">@yield('btnLabel')</button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
@endsection