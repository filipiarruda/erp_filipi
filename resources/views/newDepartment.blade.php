@extends('departmentForm')

@section('formTitle', 'Novo Departamento')

@section('btnLabel', 'Salvar departamento')
@section('route_name', route('post.newDepartment'))