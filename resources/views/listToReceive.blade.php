@extends('dashboard')

@section('title', 'Lista de contas a receber')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Contas a receber</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Tipo</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		@foreach($list as $receive)
                                @if($receive->due_date < date('Y-m-d') && $receive->received == '0' )
                                <tr class="danger">
                                @elseif($receive->received == '1')
                                <tr class="success">
                                @else
                                <tr>
                                @endif
									<td>{{ $receive->id }}</td>
									<td>{{ $receive->name }}</td>
									<td>
                                    @if($receive->payment_type == '1')
                                        Dinheiro
                                    @elseif($receive->type == '2')    
                                        Cheque
                                    @elseif($receive->type == '3')
                                        Depósito em conta
                                    @elseif($receive->type == '4')
                                        Cartão
                                    @else
                                        Boleto
                                    @endif
                                    </td>
									<td>R$ {{ number_format($receive->value, 2, ',', '.') }}</td>
                                    <td>{{implode('/', array_reverse(explode('-', $receive->due_date)))}}</td>
									<td class="text-center"><a href="{{route('form.editReceive', $receive->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
@endsection