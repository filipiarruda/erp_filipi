@extends('dashboard')

@section('title', 'Lista de departamentos')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Departamentos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Ativo</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                                <th class="text-center"><i class="fa fa-trash" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		@foreach($departments as $department)
                                @if($department->active == '0')
                                <tr class="danger">
                                @else
                                <tr>
                                @endif
									<td>{{ $department->id }}</td>
									<td>{{ $department->name }}</td>
									<td>
                                        @if($department->active == '0')
                                        Inativo
                                        @else
                                        Ativo
                                        @endif
                                    </td>
                                    <td class="text-center"><a href="{{route('form.editDepartment', $department->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
									<td class="text-center"><a href="{{route('delete.department', $department->id)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                </tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
@endsection