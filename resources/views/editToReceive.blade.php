@extends('toReceiveForm')
@section('formTitle', 'Editar conta a receber')
@section('route_name', route('post.editReceive', $receive->id))
@section('btnLabel', 'Atualizar')