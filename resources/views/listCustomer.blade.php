@extends('dashboard')

@section('title', 'Lista de Empresas')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Empresas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>city</th>
                                <th>E-mail</th>
                                <th>Telefone</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		@foreach($list as $customer)
                                <tr>
                                	<td>{{ $customer->id }}</td>
									<td>{{ $customer->name }}</td>
									<td>{{ $customer->city }}</td>
                                    <td>{{ $customer->email }}</td>
									<td>{{ $customer->telephone }}</td>
                                    <td class="text-center"><a href="{{route('form.editCustomer', $customer->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
@endsection