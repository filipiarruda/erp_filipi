@extends('departmentForm')

@section('formTitle', 'Editar Departamento')

@section('btnLabel', 'Atualizar departamento')
@section('route_name', route('post.editDepartment', $department->id))