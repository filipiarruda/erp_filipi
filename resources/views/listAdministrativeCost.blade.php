@extends('dashboard')

@section('title', 'Lista de despesas')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Despesas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Tipo</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                <th class=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                                <th class=""><i class="fa fa-trash" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		@foreach($list as $cost)
                                @if($cost->due_date < date('Y-m-d') && $cost->paid_out == '0' )
                                <tr class="danger">
                                @elseif($cost->paid_out == '1')
                                <tr class="success">
                                @else
                                <tr>
                                @endif
									<td>{{ $cost->id }}</td>
									<td>{{ $cost->name }}</td>
									<td>
                                    @if($cost->type == '1')
                                        Fixa
                                    @elseif($cost->type == '2')    
                                        Variável
                                    @elseif($cost->type == '3')
                                        Salário
                                    @else
                                        Outro
                                    @endif
                                    </td>
									<td>R$ {{ number_format($cost->value, 2, ',', '.') }}</td>
                                    <td>{{implode('/', array_reverse(explode('-', $cost->due_date)))}}</td>
                                    <td class=""><a href="{{route('form.editCost', $cost->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
									<td class=""><a href="{{route('delete.costs', $cost->id)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                </tr>
                        		@endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
@endsection