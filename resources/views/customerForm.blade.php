@extends('dashboard')

@section('title', 'Editar Funcionário')

@section('content')


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('h1')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">

                	<form method="POST" action="@yield('route_name')">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome Fantasia</label>
                    		<input type="text" class="form-control" name="name" id="name" value="{{ isset($customer->name) ? $customer->name : '' }}">
                        {{ csrf_field()}}
                        <input type="hidden" name="id" id="id" value="{{isset($customer->id) ? $customer->id : ''}}">
                    	</div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Razão Social</label>
                    		<input type="text" class="form-control" name="social_name" id="social_name" value="{{ isset($customer->social_name) ? $customer->social_name : '' }}">
                    	</div>
                    </div>
                    <div class="col-md-4">
                    	<div class="form-group">
                        <label for="">CNPJ</label>
                        <div class="input-group">
                          <input type="text" class="form-control cnpj" name="cnpj" id="cnpj" value="{{isset($customer->cnpj) ? $customer->cnpj : ''}}">
                          <div class="input-group-btn"><button class="btn btn-success" id="cnpjSearch" type="button">Buscar</button></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                    	<div class="form-group">
                        <label for="">I.E.</label>
                        <input type="text" class="form-control ie" name="ie" id="ie" value="{{ isset($customer->ie) ? $customer->ie : ''}}">
                      </div>
                    </div>
                  <div class="col-md-12">
              			<h2>Endereço Comercial</h2>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">CEP</label>
              				<div class="input-group">
              					<input type="text" class="form-control cep" name="cep" id="cep" value="{{isset($customer->cep) ? $customer->cep : ''}}">
              					<div class="input-group-btn"><button class="btn btn-success" id="cepSearch" type="button">Buscar</button></div>
              				</div>
              			</div>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">Rua, nº</label>
              				<input type="text" class="form-control" name="street" id="street" value="{{isset($customer->street) ? $customer->street : ''}}">
              			</div>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">Bairro</label>
              				<input type="text" class="form-control" name="townhouse" id="townhouse" value="{{isset($customer->townhouse) ? $customer->townhouse : ''}}">
              			</div>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">Cidade</label>
              				<input type="text" class="form-control" name="city" id="city" value="{{isset($customer->city) ? $customer->city : ''}}">
              			</div>
              		</div>
              		<div class="col-md-1">
              			<div class="form-group">
              				<label for="">UF</label>
              				<input type="text" class="form-control" name="federative_unit" id="federative_unit" value="{{isset($customer->federative_unit) ? $customer->federative_unit : ''}}">
              			</div>
              		</div>
              		<div class="col-md-12">
              			<h2>Informações de Contato</h2>
              		</div>
              		<div class="col-md-6">
              			<div class="form-group">
              				<label for="">E-mail</label>
              				<input type="text" class="form-control" id="email" name="email" value="{{isset($customer->email) ? $customer->email : ''}}">
              			</div>
              		</div>
              		<div class="col-md-6">
              			<div class="form-group">
              				<label for="">Telefone</label>
              				<input type="text" class="form-control telephone" id="telephone" name="telephone" value="{{isset($customer->telephone) ? $customer->telephone : ''}}">
              			</div>
              		</div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success" type="submit">@yield('btn')</button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
@endsection