$(document).ready(function(){

	$('input').keypress(function (e) {
        var code = null;
        code = (e.keyCode ? e.keyCode : e.which);                
        return (code == 13) ? false : true;
   });

	var url = document.location.protocol+""+document.location.hostname;
	$("#cepSearch").click(function(){
		var cep = $("#cep").val();
		$.getJSON('http://viacep.com.br/ws/'+cep+'/json/', function(response){
			$("#street").val(response.logradouro);
			$("#townhouse").val(response.bairro);
			$("#city").val(response.localidade);
			$("#federative_unit").val(response.uf);
		});
	});
	$.ajaxSetup({
 		headers : {
    		'Access-Control-Allow-Origin' : '*'
  		}
	});
	$("#cnpjSearch").click(function(){
		var cnpj = $("#cnpj").val();
		cnpjCerto = cnpj.split('.').join('');
		cnpjCerto = cnpjCerto.replace('-', '');
		cnpjCerto = cnpjCerto.replace('/', '');
		$.getJSON(document.location.protocol+''+document.location.hostname+'/cnpj/'+cnpjCerto, function(response){
			if(response.telefone != undefined || response.telefone != null){
				var tel = response.telefone.split(' /');
				var tel = tel[0];
			}else{
				var tel = '';
			}
			$("#name").val(response.fantasia);
			$("#social_name").val(response.nome);
			$("#city").val(response.municipio);
			$("#townhouse").val(response.bairro);
			$("#street").val(response.logradouro+", "+response.numero);
			$("#federative_unit").val(response.uf);
			$("#email").val(response.email);
			$("#cep").val(response.cep);
			$("#telephone").val(tel);
		});
		
	});
	$("#cep").mask('00000-000');
	$(".cpf").mask('000.000.000-00');
	$(".rg").mask('00.000.000-A');
	$(".cep").mask('00000-000');
	$('.money').mask('#.##0,00', {reverse: true});
	$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
		var SPMaskBehavior = function (val) {
  		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
  		onKeyPress: function(val, e, field, options) {
      		field.mask(SPMaskBehavior.apply({}, arguments), options);
    	}
	};
	$(".telephone").mask(SPMaskBehavior, spOptions);
	$("#dataTable").dataTable();

});

function deleteEmployee(id){
	$.post(document.location.protocol+''+document.location.hostname+'/deletar-funcionario/'+id, {id:id}, function(response){
		alert(response);
	});
}

function valueMask(value){
    value = value.toString().replace(/\D/g,"");
    value = value.toString().replace(/(\d)(\d{8})$/,"$1.$2");
    value = value.toString().replace(/(\d)(\d{5})$/,"$1.$2");
    value = value.toString().replace(/(\d)(\d{2})$/,"$1,$2");
    return value;                  
}

function deleteService(id){
	$('tr').remove("#service"+id);
}

function addService(id, name, value, decimal_value){
	var actualy = $("#services-content").html();
	var string = '<tr><td colspan="4">Insira um serviço</td></tr>';
	if(actualy == string){
		$("#services-content").html('');
		$("#services-content").append('<tr id="service'+id+'" data-id="'+id+'" data-value="'+decimal_value+'">'+'              <td>'+name+'</td>'+
                                            '<td>'+value+'</td>'+
	                                        '<td><a><i class="fa fa-edit"></i></a></td>'+
                                            '<td><a onclick="deleteService(\''+id+'\');"><i class="fa fa-trash"></i></a></td>'+
                                        '</tr>');

	}else{
		if($("#service"+id).length == 0){
			$("#services-content").append('<tr id="service'+id+'" data-id="'+id+'" data-value="'+decimal_value+'">'+'              <td>'+name+'</td>'+
                                            '<td>'+value+'</td>'+
	                                        '<td><a href="#"><i class="fa fa-edit"></i></a></td>'+
                                            '<td><a ><i class="fa fa-trash"></i></a></td>'+
                                        '</tr>');
		}else{
			var newValue1 = parseFloat($("#service"+id).attr('data-value')) + parseFloat(decimal_value);
			newValue = valueMask(newValue1.toFixed(2));
			$("#service"+id).html('<td>'+name+'</td>'+
                                            '<td>R$ '+newValue+'</td>'+
	                                        '<td><a href="#"><i class="fa fa-edit"></i></a></td>'+
                                            '<td><a onclick="deleteService(\''+id+'\');"><i class="fa fa-trash"></i></a></td>');
			$('#service'+id).attr('data-value', newValue1.toFixed(2));
		}
	}
}

function addServices(id){
	var actualy = $("#services_concat").val;
	var now = actualy+','+id;
	$("#services_concat").val(now);
	$("#services-content").append('<tr>'+'              <td>Serviço teste</td>'+
                                            '<td>R$ 2.500,00</td>'+
	                                        '<td><a href="#"><i class="fa fa-edit"></i></a></td>'+
                                            '<td><a href="#"><i class="fa fa-trash"></i></a></td>'+
                                        '</tr>');
}
function cleanServices(){
	$("#services-content").html('<tr><td colspan="4">Insira um serviço</td></tr>');
}