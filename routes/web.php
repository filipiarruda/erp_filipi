<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Login Route
/*Route::get('/', function () {
    return view('welcome');
});*/

//Dashboard route
Route::get('/dashboard', ['as'=>'dashboard', 'uses'=>'HomeController@dashboard']);

//Employees routes
Route::get('/nosso-grupo', 'companyController@list');
Route::get('/novo-funcionario', ['as'=>'form.newEmployee', 'uses'=>'employeeController@newEmployee']);
Route::get('/funcionarios', ['uses'=>'employeeController@listEmployee', 'as'=>'listEmployees']);
Route::post('novo-funcionario', ['as'=>'post.insertEmployee', 'uses'=>'employeeController@insertEmployee']);
Route::post('/atualizar-funcionario', ['as'=>'post.updateEmployee', 'uses'=>'employeeController@updateEmployee']);
Route::get('/editar-funcionario/{id}', ['as'=>'form.editEmployee', 'uses'=>'employeeController@editEmployee']);

//Departments routes
Route::get('/novo-departamento', ['as'=>'form.newDepartment', 'uses'=>'departmentController@newDepartment']);
Route::get('/editar-departamento/{id?}', ['as'=>'form.editDepartment', 'uses'=>'departmentController@editDepartment']);
Route::post('/novo-departamento', ['as'=>'post.newDepartment', 'uses'=>'departmentController@saveDepartment']);
Route::post('/editar-departamento/{id}', ['as'=>'post.editDepartment', 'uses'=>'departmentController@updateDepartment']);
Route::get('/departamentos', ['as'=>'list.departments', 'uses'=>'departmentController@listDepartment']);
Route::get('/deletar-departamento/{id}', ['as'=>'delete.department', 'uses'=>'departmentController@deleteDepartment']);

//Customer routes
Route::get('/nova-empresa', ['as'=>'form.insertCustomer', 'uses'=>'customerController@insertForm']);
Route::get('/editar-empresa/{id}', ['as'=>'form.editCustomer', 'uses'=>'customerController@editForm']);
Route::post('/nova-empresa', ['as'=>'post.insertCustomer', 'uses'=>'customerController@saveCustomer']);
Route::get('/empresas', ['as'=>'list.customer', 'uses'=>'customerController@list']);
Route::post('/editar-empresa/{id}', ['as'=>'post.editCustomer', 'uses'=>'customerController@updateCustomer']);
Route::get('/cnpj/{cnpj}', ['as'=>'get.Cnpj', 'uses'=>'customerController@returnCnpj']);
Auth::routes();

//Administrative costs routes
Route::get('/nova-despesa', ['as'=>'form.insertCost', 'uses'=>'administrativeCostsController@insertForm']);
Route::post('/nova-despesa', ['as'=>'post.insertCost', 'uses'=>'administrativeCostsController@saveCost']);
Route::get('/editar-despesa/{id}', ['as'=>'form.editCost', 'uses'=>'administrativeCostsController@editForm']);
Route::post('/editar-despesa/{id}', ['as'=>'post.editCost', 'uses'=>'administrativeCostsController@updateCost']);
Route::get('/despesas', ['as'=>'list.costs', 'uses'=>'administrativeCostsController@list']);
Route::get('/deletar-despesa/{id}', ['as'=>'delete.costs', 'uses'=>'administrativeCostsController@delete_cost']);

//To receive routes
Route::get('/nova-conta-a-receber', ['as'=>'form.insertReceive', 'uses'=>'toReceiveController@insertForm']);
Route::post('/nova-conta-a-receber', ['as'=>'post.insertReceive', 'uses'=>'toReceiveController@saveReceive']);
Route::get('/editar-conta-a-receber/{id}', ['as'=>'form.editReceive', 'uses'=>'toReceiveController@editForm']);
Route::post('/editar-conta-a-receber/{id}', ['as'=>'post.editReceive', 'uses'=>'toReceiveController@updateReceive']);
Route::get('/contas-a-receber', ['as'=>'list.receive', 'uses'=>'toReceiveController@list']);

//Services routes
Route::get('/novo-servico', ['as'=>'form.insertService', 'uses'=>'serviceController@insertForm']);
Route::post('/novo-servico', ['as'=>'post.insertService', 'uses'=>'serviceController@saveService']);
Route::get('/editar-servico/{id}', ['as'=>'form.editService', 'uses'=>'serviceController@editService']);
Route::get('/deletar-servico/{id}', ['as'=>'delete.service', 'uses'=>'serviceController@deleteService']);
Route::get('/servicos', ['as'=>'list.service', 'uses'=>'serviceController@list']);
Route::post('/editar-servico/{id}', ['as'=>'post.editService', 'uses'=>'serviceController@updateService']);

//Contracts routes
Route::get('/novo-contrato', ['as'=>'form.insertContract', 'uses'=>'contractController@insertForm']);

Route::get('/', 'HomeController@index');
Route::get('/logout', ['as'=>'user.logout' ,'uses'=>'HomeController@logOut']);
