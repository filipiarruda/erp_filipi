<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\company;
class companyController extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }

    public function insertCompany(Request $req){

    	$company = new company;
    	$company->name = $req->name;
    	$company->active = $req->active;
    	$company->created_at = time();
    	$company->save();
    }

    public function list(){
    	$list = company::all();
    	return view('listCompany', compact('list'));
    }
}
