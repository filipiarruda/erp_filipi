<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\administrative_costs;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;
use App\company;
class administrativeCostsController extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }
    
    public function insertForm(){
        //$route = route('post.insertCustomer');
        $user = $user = Auth::user();
        $companies = company::all();
        return view('newAdministrativeCost', compact('user', 'companies'));
    }

    public function editForm($id){
        $cost = administrative_costs::find($id);
        $companies = company::all();
        $user = $user = Auth::user();
        //$route = route('post.editCustomer');
        return view('editAdministrativeCost', compact('cost', 'user', 'companies'));
    }

    public function returnCnpj($cnpj){
        $resp = file_get_contents('https://www.receitaws.com.br/v1/cnpj/'.$cnpj);
        $resp = json_decode($resp, true);
        return response()->json($resp);
    }

    public function getAmount($money)
{
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return number_format(str_replace(',', '.', $removedThousendSeparator), 2, '.', '');
    }
    public function saveCost(Request $req){
        $user = $user = Auth::user();
        $cost = new administrative_costs;
        $cost->name = $req->name;

        $cost->value = $this->getAmount($req->value);
        $cost->type = $req->type;
        $cost->due_date = $req->due_date;
        $cost->date_payment = $req->payment_date == '' ? null : $req->payment_date;
        $cost->paid_out =  $cost->date_payment <> '' ? '1' : '0';
        $cost->user_id = $user->id;
        $cost->company_id = $req->company_id;
        //$customer->active = $req->active;
        //$customer->created_at = time();
        $cost->save();
        $route = route('list.costs');
        return redirect($route);
    }

    public function updateCost(Request $req, $id){
        $user = $user = Auth::user();
        $cost = administrative_costs::find($id);
        $cost->name = $req->name;

        $cost->value = $this->getAmount($req->value);
        $cost->type = $req->type;
        $cost->due_date = $req->due_date;
        $cost->date_payment = $req->payment_date;
        $cost->paid_out =  $cost->date_payment <> '' ? '1' : '0';
        $cost->user_id = $user->id;
        $cost->company_id = $req->company_id;
        //$customer->active = $req->active;
        //$customer->created_at = time();
        $cost->save();
        $route = route('list.costs');
        return redirect($route);
    }

    public function list(){
        $user = Auth::user();
    	$list = administrative_costs::all();
    	return view('listAdministrativeCost', compact('list', 'user'));
    }

    public function delete_cost($id){
        $user = Auth::user();
        $list = administrative_costs::all();
        $cost = administrative_costs::find($id);
        $cost->delete();
        $route = route('list.costs');
        return redirect($route);
    }
}
