<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\employee;
use App\company;
use Illuminate\Support\Facades\Hash;
class employeeController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }

    public function newEmployee(){
        $user = $user = Auth::user();
        $companies = company::all();
        return view('funcionarioNovo', compact('companies', 'user'));
    }
    public function insertEmployee(Request $req){

    	$employee = new employee;
    	$employee->name = $req->name;
    	$employee->office_id = $req->office_id;
    	$employee->rg = $req->rg;
    	$employee->cpf = $req->cpf;
    	$employee->login = $req->login;
        $employee->company_id = $req->company_id;
    	$employee->password = Hash::make('123456', ['rounds' => 12]);
    	$employee->user_id = $user->id;
    	$employee->department = "0";
    	$employee->admission_date = $req->admission_date;
    	$employee->resignation_date = $req->resignation_date <> '' ? $req->resignation_date : '0001-01-01';
    	$employee->birth_date = $req->birth_date;
    	$employee->date_register = date('Y-m-d');
    	$employee->salary = $req->salary;
    	$employee->percent = str_replace(',', '.', $req->percent);
    	$employee->street = $req->street;
    	$employee->city = $req->city;
    	$employee->townhouse = $req->townhouse;
    	$employee->federative_unit = $req->federative_unit;
    	$employee->telephone = $req->telephone;
    	$employee->email = $req->email;
    	$employee->payment_date = '10';
    	$employee->cep = $req->cep;
    	$employee->photo = 'none';
    	$employee->permissions = 'adm';
    	$employee->save();
    	return redirect('listEmployee');
    }

    public function listEmployee(){
        $user = $user = Auth::user();
    	$list = employee::all();
    	return view('listEmployee', compact('list', 'user'));
    }


    public function editEmployee($id){
        $employee = employee::find($id);
        $companies = company::all();
        $user = $user = Auth::user();
        return view('editEmployee', compact('employee', 'companies', 'user'));
    }

    public function updateEmployee(Request $req){
        $employee = employee::find($req->id);
        $employee->name = $req->name;
        $employee->office_id = $req->office_id;
        $employee->rg = $req->rg;
        $employee->cpf = $req->cpf;
        $employee->login = $req->login;
        // $employee->password = Hash::make('123456', ['rounds' => 12]);
        //$employee->user_id = "0";
        $employee->department = "0";
        $employee->company_id = $req->company_id;
        $employee->admission_date = $req->admission_date;

        $employee->resignation_date = $req->resignation_date <> '' ? $req->resignation_date : '0001-01-01';
        $employee->birth_date = $req->birth_date;
        $employee->date_register = date('Y-m-d');
        $employee->salary =  $req->salary;
        $employee->percent = str_replace(',', '.', $req->percent);
        $employee->street = $req->street;
        $employee->city = $req->city;
        $employee->townhouse = $req->townhouse;
        $employee->federative_unit = $req->federative_unit;
        $employee->telephone = $req->telephone;
        $employee->email = $req->email;
        $employee->payment_date = '10';
        $employee->cep = $req->cep;
        $employee->photo = 'none';
        $employee->permissions = 'adm';
        $employee->save();
        $redirect = route('listEmployees');
        return redirect($redirect); 
    }

    public function deleteEmployee(Request $id){
        $employee = employee::find($id)->delete();
        if($employee->trashed()){
            return "1";
        }else{
            return "0";
        }
    }
}
