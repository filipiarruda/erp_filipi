<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\to_receive;
use App\customer;
use App\company;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;
class toReceiveController extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }
    
    public function insertForm(){
        //$route = route('post.insertCustomer');
        $user = $user = Auth::user();
        $customers = customer::all();
        $companies = company::all();
        return view('newToReceive', compact('user', 'customers', 'companies'));
    }

    public function editForm($id){
        $receive = to_receive::find($id);
        $user = $user = Auth::user();
        $customers = customer::all();
        $companies = company::all();
        //$route = route('post.editCustomer');
        return view('editToReceive', compact('receive', 'user', 'customers', 'companies'));
    }

    public function returnCnpj($cnpj){
        $resp = file_get_contents('https://www.receitaws.com.br/v1/cnpj/'.$cnpj);
        $resp = json_decode($resp, true);
        return response()->json($resp);
    }

    public function getAmount($money)
{
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return number_format(str_replace(',', '.', $removedThousendSeparator), 2, '.', '');
    }
    public function saveReceive(Request $req){
        $user = $user = Auth::user();
        $receive = new to_receive;
        $receive->name = $req->name;
        $receive->value = $this->getAmount($req->value);
        $receive->payment_type = $req->payment_type;
        $receive->due_date = $req->due_date;
        $receive->received_date = $req->received_date <> '' ? $req->received_date : null;
        $receive->received = $receive->received_date <> '' ? '1' : '0';
        $receive->user_id = $user->id;
        $receive->customer_id = $req->customer_id <> '' ? $req->customer_id : null;
        $receive->company_id = $req->company_id;
        $receive->save();
        $route = route('list.receive');
        return redirect($route);
    }

    public function updateReceive(Request $req, $id){
        $user = $user = Auth::user();
        $receive = to_receive::find($id);
        $receive->name = $req->name;
        $receive->value = $this->getAmount($req->value);
        $receive->payment_type = $req->payment_type;
        $receive->due_date = $req->due_date;
        $receive->received_date = $req->received_date <> '' ? $req->received_date : null;
        $receive->received = $receive->received_date <> '' ? '1' : '0';
        $receive->user_id = $user->id;
        $receive->customer_id = $req->customer_id <> '' ? $req->customer_id : null;
        $receive->company_id = $req->company_id;
        $receive->save();
        $route = route('list.receive');
        return redirect($route);
    }

    public function list(){
        $user = $user = Auth::user();
    	$list = to_receive::all();
    	return view('listToReceive', compact('list', 'user'));
    }

    public function deleteReceive($id){
        $user = $user = Auth::user();
        $receive = to_receive::find($id);
        $receive->delete();
        $route = route('list.receive');
        return redirect($route);
    }
}
