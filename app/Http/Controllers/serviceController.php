<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\department;
use App\company;
use App\service;

class serviceController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function insertForm(){
    	$departments = department::all();
    	$companies = company::all();
    	return view('insertService', compact('departments', 'companies'));
    }
    public function getAmount($money){
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return number_format(str_replace(',', '.', $removedThousendSeparator), 2, '.', '');
    }
    public function saveService(Request $req){
    	$service = new service();
    	$user = Auth::user();
    	$service->name = $req->name;
    	$service->department_id = $req->department_id;
    	$service->company_id = $req->company_id;
    	$service->user_id = $user->id;
    	$service->active = $req->active;
    	$service->value = $this->getAmount($req->value);
    	$service->save();
    	$route = route('list.service');
    	return redirect($route);
    }

    public function list(){
    	$services = service::all();
    	return view('listService', compact('services'));
    }

    public function editService($id){
    	$service = service::find($id);
    	$departments = department::all();
    	$companies = company::all();
    	return view('editService', compact('service', 'companies', 'departments'));
    }

    public function updateService(Request $req, $id){
    	$service = service::find($id);
    	$service->name = $req->name;
    	$service->department_id = $req->department_id;
    	$service->company_id = $req->company_id;
    	//$service->user_id = $user->id;
    	$service->active = $req->active;
    	$service->value = $this->getAmount($req->value);
    	$service->save();
    	$route = route('list.service');
    	return redirect($route);
    }

    public function deleteService($id){
    	$service = service::find($id);
    	$service->delete();
    	$route = route('list.service');
    	return redirect($route);
    }
}
