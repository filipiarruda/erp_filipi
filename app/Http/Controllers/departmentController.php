<?php

namespace App\Http\Controllers;
use App\department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class departmentController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    
    public function newDepartment(){
        $user = $user = Auth::user();
    	return view('newDepartment', compact('user'));
    }
    
    public function editDepartment($id = null){
        $user = $user = Auth::user();
        $department = department::find($id);
    	return view('editDepartment', compact('department', 'user'));
    }

    public function saveDepartment(Request $req){
        $department = new department;
        $department->name = $req->name;
        $department->active = $req->active;
        $department->save();
        $route = route('list.departments');
        return redirect($route);
    }

    public function updateDepartment(Request $req, $id){
    	$department = department::find($id);
    	$department->name = $req->name;
    	$department->active = $req->active;
    	$department->save();
        $route = route('list.departments');
    	return redirect($route);
    }

    public function listDepartment(){
    	$departments = department::all();
        $user = $user = Auth::user();
    	return view('listDepartment', compact('departments', 'user'));
    }

    public function deleteDepartment($id){
    	$department = department::find($id);
    	$department->delete();
    	$return = route('list.departments');
    	return redirect($return);
    }
}
