<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\employee;
use App\department;
use App\to_receive;
use App\administrative_costs;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('dashboard');
    }

    public function logOut(){
        Auth::logout();
        return view('auth.login');
    }
    public function dashboard(){
        $user = $user = Auth::user();
        $employees = employee::where(['resignation_date' => null])->count();
        $departments = department::where(['active' => '1'])->count();
        $toReceive = to_receive::where(['received' => '0'])->count();
        $costs = administrative_costs::where(['paid_out' => '0'])->count();
        $received = to_receive::allReceived();
        $received_v4 = to_receive::v4Received();
        $received_al = to_receive::alReceived();
        $received_valley = to_receive::valleyReceived();
        $payed = administrative_costs::allPayed();
        $payed_v4 = administrative_costs::v4Payed();
        $payed_al = administrative_costs::alPayed();
        $payed_valley = administrative_costs::valleyPayed();
        $result = $received - $payed;
        $result_v4 = $received_v4 - $payed_v4;
        $result_al = $received_al - $payed_al;
        $result_valley = $received_valley - $payed_valley;
        return view('panel', compact('user', 'employees', 'departments', 'toReceive', 'costs', 'received', 'payed', 'result', 'received_v4', 'payed_v4', 'result_v4', 'received_al', 'payed_al', 'result_al', 'received_valley', 'payed_valley', 'result_valley'));
    }
}
