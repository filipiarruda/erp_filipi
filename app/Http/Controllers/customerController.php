<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\customer;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;
class customerController extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }
    
    public function insertForm(){
        //$route = route('post.insertCustomer');
        $user = $user = Auth::user();
        return view('insertCustomer', compact('user'));
    }

    public function editForm($id){
        $customer = customer::find($id);
        $user = $user = Auth::user();
        //$route = route('post.editCustomer');
        return view('editCustomer', compact('customer', 'user'));
    }

    public function returnCnpj($cnpj){
        $resp = file_get_contents('https://www.receitaws.com.br/v1/cnpj/'.$cnpj);
        $resp = json_decode($resp, true);
        return response()->json($resp);
    }

    public function saveCustomer(Request $req){
        $user = $user = Auth::user();
        $customer = new customer;
        $customer->name = $req->name;
        $customer->social_name = $req->social_name;
        $customer->cnpj = $req->cnpj;
        $customer->ie = $req->ie;
        $customer->cep = $req->cep;
        $customer->street = $req->street;
        $customer->townhouse = $req->townhouse;
        $customer->city = $req->city;
        $customer->federative_unit = $req->federative_unit;
        $customer->email = $req->email;
        $customer->telephone = $req->telephone;
        $customer->user_id = $user->id;
        //$customer->active = $req->active;
        //$customer->created_at = time();
        $customer->save();
        return redirect('dashboard');
    }

    public function updateCustomer(Request $req){
        $customer = customer::find($req->id);
    	$customer->name = $req->name;
        $customer->social_name = $req->social_name;
        $customer->cnpj = $req->cnpj;
        $customer->ie = $req->ie;
        $customer->cep = $req->cep;
        $customer->street = $req->street;
        $customer->townhouse = $req->townhouse;
        $customer->city = $req->city;
        $customer->federative_unit = $req->federative_unit;
        $customer->email = $req->email;
        $customer->telephone = $req->telephone;
        //$customer->user_id = $user->id;
    	//$customer->active = $req->active;
    	//$customer->created_at = time();
    	$customer->save();
        $route = route('list.customer');
        return redirect($route);
    }

    public function list(){
        $user = $user = Auth::user();
    	$list = customer::all();
    	return view('listCustomer', compact('list', 'user'));
    }
}
