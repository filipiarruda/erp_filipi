<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\company;
use App\employee;
use App\service;
use App\customer;
use Illuminate\Support\Facades\Auth;
class contractController extends Controller
{
    public function insertForm(){
    	$user = Auth::user();
    	$employees = employee::all();
    	$customers = customer::all();
    	$services = service::all();
    	return view('insertContract', compact('employees', 'customers', 'user', 'services'));
    }
}
