<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class administrative_costs extends Model
{
    public static function allPayed(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = administrative_costs::select('date_payment','value')->whereBetween('date_payment',[$dateA,$dateB])->sum('value');
    	return $total;
    }

    public static function v4Payed(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = administrative_costs::select('date_payment','value', 'company_id')->whereBetween('date_payment',[$dateA,$dateB])->where('company_id', '1')->sum('value');
    	return $total;
    }

    public static function alPayed(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = administrative_costs::select('date_payment','value', 'company_id')->whereBetween('date_payment',[$dateA,$dateB])->where('company_id', '2')->sum('value');
    	return $total;
    }

    public static function valleyPayed(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = administrative_costs::select('date_payment','value', 'company_id')->whereBetween('date_payment',[$dateA,$dateB])->where('company_id', '3')->sum('value');
    	return $total;
    }
}
