<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class to_receive extends Model
{
    public static function allReceived(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = to_receive::select('received_date','value')->whereBetween('received_date',[$dateA,$dateB])->sum('value');
    	return $total;
    }

    public static function v4Received(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = to_receive::select('received_date','value', 'company_id')->whereBetween('received_date',[$dateA,$dateB])->where('company_id', '1')->sum('value');
    	return $total;
    }

    public static function alReceived(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = to_receive::select('received_date','value', 'company_id')->whereBetween('received_date',[$dateA,$dateB])->where('company_id', '2')->sum('value');
    	return $total;
    }

    public static function valleyReceived(){
    	$dateA = date('Y-m-d', strtotime('first day of this month', time()));
    	$dateB = date('Y-m-d', strtotime('last day of this month', time()));
    	$total = to_receive::select('received_date','value', 'company_id')->whereBetween('received_date',[$dateA,$dateB])->where('company_id', '3')->sum('value');
    	return $total;
    }
}
