<?php $__env->startSection('title', 'Empresas do grupo'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de Empresas do Grupo</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Ativo</th>
                        	</thead>
                        	<tbody>
                        		<?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
								<tr>
									<td><?php echo e($company->id); ?></td>
									<td><?php echo e($company->name); ?></td>
									<?php if($company->active == 'true' || $company->active == 1): ?>
									<td>Sim</td>
									<?php endif; ?>
									<?php if($company->active == 'false' || $company->active == 0): ?>
									<td>Não</td>
									<?php endif; ?>
								</tr>
                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>