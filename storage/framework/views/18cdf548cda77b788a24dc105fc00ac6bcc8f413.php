<?php $__env->startSection('title', 'Lista de despesas'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Despesas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Tipo</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		<?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cost): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php if($cost->due_date < date('Y-m-d') && $cost->paid_out == '0' ): ?>
                                <tr class="danger">
                                <?php elseif($cost->paid_out == '1'): ?>
                                <tr class="success">
                                <?php else: ?>
                                <tr>
                                <?php endif; ?>
									<td><?php echo e($cost->id); ?></td>
									<td><?php echo e($cost->name); ?></td>
									<td>
                                    <?php if($cost->type == '1'): ?>
                                        Fixa
                                    <?php elseif($cost->type == '2'): ?>    
                                        Variável
                                    <?php elseif($cost->type == '3'): ?>
                                        Salário
                                    <?php else: ?>
                                        Outro
                                    <?php endif; ?>
                                    </td>
									<td>R$ <?php echo e(number_format($cost->value, 2, ',', '.')); ?></td>
                                    <td><?php echo e(implode('/', array_reverse(explode('-', $cost->due_date)))); ?></td>
									<td class="text-center"><a href="<?php echo e(route('form.editCost', $cost->id)); ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>