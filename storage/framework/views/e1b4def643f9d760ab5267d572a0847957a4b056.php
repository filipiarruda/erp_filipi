<?php $__env->startSection('formTitle', 'Editar despesa'); ?>
<?php $__env->startSection('route_name', route('post.editCost', $cost->id)); ?>
<?php $__env->startSection('btnLabel', 'Atualizar'); ?>
<?php echo $__env->make('administrativeCostsForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>