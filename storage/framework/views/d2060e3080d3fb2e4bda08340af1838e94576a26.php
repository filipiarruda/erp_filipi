<?php $__env->startSection('formTitle', 'Novo Departamento'); ?>

<?php $__env->startSection('btnLabel', 'Salvar departamento'); ?>
<?php $__env->startSection('route_name', route('post.newDepartment')); ?>
<?php echo $__env->make('departmentForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>