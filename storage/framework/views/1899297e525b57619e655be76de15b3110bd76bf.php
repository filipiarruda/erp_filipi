<?php $__env->startSection('title', 'Lista de Empresas'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Empresas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>city</th>
                                <th>E-mail</th>
                                <th>Telefone</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		<?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <tr>
                                	<td><?php echo e($customer->id); ?></td>
									<td><?php echo e($customer->name); ?></td>
									<td><?php echo e($customer->city); ?></td>
                                    <td><?php echo e($customer->email); ?></td>
									<td><?php echo e($customer->telephone); ?></td>
                                    <td class="text-center"><a href="<?php echo e(route('form.editCustomer', $customer->id)); ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>