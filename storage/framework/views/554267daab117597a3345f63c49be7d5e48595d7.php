<?php $__env->startSection('title', 'Serviços'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $__env->yieldContent('formTitle'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                	<form method="POST" action="<?php echo $__env->yieldContent('route_name'); ?>">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="<?php echo e(isset($service->name) ? $service->name : ''); ?>">
                        <?php echo e(csrf_field()); ?>

                    	</div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Ativo</label>
                    		<select name="active" id="active" class="form-control">
                          <?php if(isset($service->active) && $service->active == '1'): ?>
                          <option value="1" selected>Sim</option>
                          <option value="0">Não</option>
                          <?php elseif(isset($service->active) && $service->active == '0'): ?>
                          <option value="1">Sim</option>
                          <option value="0" selected>Não</option>
                          <?php else: ?>
                          <option value="1">Sim</option>
                          <option value="0">Não</option>
                          <?php endif; ?>
                        </select>
                    	</div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Departamento</label>
                        <select name="department_id" id="department_id" class="form-control">
                          <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                          <option value="<?php echo e($department->id); ?>" <?php echo e(isset($service->department_id) && $service->department_id == $department->id ? 'selected' : ''); ?>><?php echo e($department->name); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Empresa que realiza o serviço</label>
                        <select name="company_id" id="company_id" class="form-control">
                          <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                          <option value="<?php echo e($company->id); ?>" <?php echo e(isset($service->company_id) && $service->company_id == $company->id ? 'selected' : ''); ?>><?php echo e($company->name); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Valor</label>
                        <div class="input-group">
                          <div class="input-group-addon">R$</div>
                          <input type="text" class="form-control money" id="value" name="value" placeholder="0.000,00" value="<?php echo e(isset($service->value) ? number_format($service->value, 2, ',', '.') : ''); ?>">
                  </div>
                      </div>
                    </div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success"><?php echo $__env->yieldContent('btnLabel'); ?></button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>