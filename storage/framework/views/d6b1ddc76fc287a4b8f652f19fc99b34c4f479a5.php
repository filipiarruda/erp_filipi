<?php $__env->startSection('formTitle', 'Editar serviço'); ?>
<?php $__env->startSection('route_name', route('post.editService', $service->id)); ?>
<?php $__env->startSection('btnLabel', 'Atualizar'); ?>
<?php echo $__env->make('serviceForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>