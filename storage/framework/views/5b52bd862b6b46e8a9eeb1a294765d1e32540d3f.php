<?php $__env->startSection('title', 'Lista de contas a receber'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Contas a receber</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Tipo</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		<?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $receive): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php if($receive->due_date < date('Y-m-d') && $receive->received == '0' ): ?>
                                <tr class="danger">
                                <?php elseif($receive->received == '1'): ?>
                                <tr class="success">
                                <?php else: ?>
                                <tr>
                                <?php endif; ?>
									<td><?php echo e($receive->id); ?></td>
									<td><?php echo e($receive->name); ?></td>
									<td>
                                    <?php if($receive->payment_type == '1'): ?>
                                        Dinheiro
                                    <?php elseif($receive->type == '2'): ?>    
                                        Cheque
                                    <?php elseif($receive->type == '3'): ?>
                                        Depósito em conta
                                    <?php elseif($receive->type == '4'): ?>
                                        Cartão
                                    <?php else: ?>
                                        Boleto
                                    <?php endif; ?>
                                    </td>
									<td>R$ <?php echo e(number_format($receive->value, 2, ',', '.')); ?></td>
                                    <td><?php echo e(implode('/', array_reverse(explode('-', $receive->due_date)))); ?></td>
									<td class="text-center"><a href="<?php echo e(route('form.editReceive', $receive->id)); ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>