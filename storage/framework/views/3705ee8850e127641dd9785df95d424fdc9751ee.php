<?php $__env->startSection('title', 'Lista de departamentos'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Departamentos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Ativo</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                                <th class="text-center"><i class="fa fa-trash" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		<?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php if($department->active == '0'): ?>
                                <tr class="danger">
                                <?php else: ?>
                                <tr>
                                <?php endif; ?>
									<td><?php echo e($department->id); ?></td>
									<td><?php echo e($department->name); ?></td>
									<td>
                                        <?php if($department->active == '0'): ?>
                                        Inativo
                                        <?php else: ?>
                                        Ativo
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center"><a href="<?php echo e(route('form.editDepartment', $department->id)); ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
									<td class="text-center"><a href="<?php echo e(route('delete.department', $department->id)); ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                </tr>
                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>