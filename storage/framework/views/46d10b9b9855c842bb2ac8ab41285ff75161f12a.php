<?php $__env->startSection('title', 'Editar Funcionário'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastrar novo Funcionário</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">

                	<form method="POST" action="<?php echo e(route('post.updateEmployee')); ?>">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="<?php echo e(isset($employee->name) ? $employee->name : ''); ?>">
                        <input type="hidden" name="id" id="id" value="<?php echo e(isset($employee->id) ? $employee->id : ''); ?>">
                    	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group">
                    		<label for="">Login</label>
                    		<input type="text" class="form-control" name="login" id="login" value="<?php echo e(isset($employee->login) ? $employee->login : ''); ?>">
                    	</div>
                    </div>
                    <div class="col-md-3">
                    	<div class="form-group">
                    		<label for="">Senha</label>
                    		<input type="password" class="form-control" name="password" id="password" value="">
                    		<?php echo e(csrf_field()); ?>

                    	</div>
                    </div>
                    <div class="col-md-4">
                    	<div class="form-group">
                    		<label for="">Cargo</label>
                    		<input type="text" class="form-control" name="office_id" id="office_id" value="<?php echo e(isset($employee->office_id) ? $employee->office_id : ''); ?>">
                    	</div>
                    </div>
                    <div class="col-md-4">
                    	<div class="form-group">
                        <label for="">RG</label>
                        <input type="text" class="form-control rg" name="rg" id="rg" value="<?php echo e(isset($employee->rg) ? $employee->rg : ''); ?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                    	<div class="form-group">
                        <label for="">CPF</label>
                        <input type="text" class="form-control cpf" name="cpf" id="cpf" value="<?php echo e(isset($employee->cpf) ? $employee->cpf : ''); ?>">
                      </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    		<div class="col-md-4">
                    			<div class="form-group">
                            <label for="">Data de Admissão</label>
                            <input type="date" class="form-control" name="admission_date" id="admission_date" value="<?php echo e(isset($employee->admission_date) ? $employee->admission_date : '0000-00-00'); ?>">
                          </div>
                    		</div>
                    		<div class="col-md-4">
                    			<div class="form-group">
                    				<label for="">Data de demissão</label>
                    				<input type="date" class="form-control" name="resignation_date" id="resignation_date" aria-describeby="helpResignation" value="<?php echo e(isset($employee->resignation_date) ? $employee->resignation_date : '0000-00-00'); ?>">
									<small id="helpResignation" class="form-text text-muted">Após colocar data de demissão, funcionário não terá mais acesso ao sistema.</small>
                    			</div>
                    		</div>
                    		<div class="col-md-4">
                    			<div class="form-group">
                    				<label for="">Data de nascimento</label>
                    				<input type="date" class="form-control" name="birth_date" id="birth_date" value="<?php echo e(isset($employee->birth_date) ? $employee->birth_date : '0000-00-00'); ?>">
                    			</div>
                    		</div>
                		</div>
                	</div>
                    <div class="col-md-4">
                    	<div class="form-group"><label for="">Salário</label>
                    		<div class="input-group">
        						<div class="input-group-addon">R$</div>
        						<input type="text" class="form-control money" id="salary" name="salary" placeholder="0.000,00" value="<?php echo e(isset($employee->salary) ? $employee->salary : ''); ?>">
      						</div>
                   		</div>
              		</div>
              		<div class="col-md-4">
                    	<div class="form-group"><label for="">Percentual de Lucro</label>
                    		<div class="input-group">
        						<input type="text" class="form-control" id="percent" name="percent" placeholder="10" value="<?php echo e(isset($employee->percent) ? $employee->percent : ''); ?>">
        						<div class="input-group-addon">%</div>
      						</div>
                   		</div>
              		</div>
                  <div class="col-md-4">
                    <div class="form-group"><label for="">Empresa</label>
                      <select name="company_id" id="company_id" class="form-control">
                        <option value="">Selecione</option>
                        <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                          <?php if($employee->company_id == $company->id): ?>
                            <option value="<?php echo e($company->id); ?>" selected><?php echo e($company->name); ?></option>
                          <?php else: ?>
                            <option value="<?php echo e($company->id); ?>"><?php echo e($company->name); ?></option>
                          <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                      </select>
                    </div>
                  </div>
              		<div class="col-md-12">
              			<h2>Endereço</h2>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">CEP</label>
              				<div class="input-group">
              					<input type="text" class="form-control cep" name="cep" id="cep" value="<?php echo e(isset($employee->cep) ? $employee->cep : ''); ?>">
              					<div class="input-group-btn"><button class="btn btn-success" id="cepSearch">Buscar</button></div>
              				</div>
              			</div>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">Rua, nº</label>
              				<input type="text" class="form-control" name="street" id="street" value="<?php echo e(isset($employee->street) ? $employee->street : ''); ?>">
              			</div>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">Bairro</label>
              				<input type="text" class="form-control" name="townhouse" id="townhouse" value="<?php echo e(isset($employee->townhouse) ? $employee->townhouse : ''); ?>">
              			</div>
              		</div>
              		<div class="col-md-4">
              			<div class="form-group">
              				<label for="">Cidade</label>
              				<input type="text" class="form-control" name="city" id="city" value="<?php echo e(isset($employee->city) ? $employee->city : ''); ?>">
              			</div>
              		</div>
              		<div class="col-md-1">
              			<div class="form-group">
              				<label for="">UF</label>
              				<input type="text" class="form-control" name="federative_unit" id="federative_unit" value="<?php echo e(isset($employee->federative_unit) ? $employee->federative_unit : ''); ?>">
              			</div>
              		</div>
              		<div class="col-md-12">
              			<h2>Informações de Contato</h2>
              		</div>
              		<div class="col-md-6">
              			<div class="form-group">
              				<label for="">E-mail</label>
              				<input type="text" class="form-control" id="email" name="email" value="<?php echo e(isset($employee->email) ? $employee->email : ''); ?>">
              			</div>
              		</div>
              		<div class="col-md-6">
              			<div class="form-group">
              				<label for="">Telefone</label>
              				<input type="text" class="form-control telephone" id="telephone" name="telephone" value="<?php echo e(isset($employee->telephone) ? $employee->telephone : ''); ?>">
              			</div>
              		</div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success" type="submit">Atualizar Funcionário</button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>