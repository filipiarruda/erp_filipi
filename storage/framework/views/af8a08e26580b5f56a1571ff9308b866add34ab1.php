<?php $__env->startSection('formTitle', 'Editar Departamento'); ?>

<?php $__env->startSection('btnLabel', 'Atualizar departamento'); ?>
<?php $__env->startSection('route_name', route('post.editDepartment', $department->id)); ?>
<?php echo $__env->make('departmentForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>