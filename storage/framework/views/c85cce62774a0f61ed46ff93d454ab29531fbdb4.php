<?php $__env->startSection('title', 'Departamento'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $__env->yieldContent('formTitle'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                	<form method="POST" action="<?php echo $__env->yieldContent('route_name'); ?>">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="<?php echo e(isset($department->name) ? $department->name : ''); ?>">
                        <?php echo e(csrf_field()); ?>

                    	</div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Ativo</label>
                    		<select name="active" id="active" class="form-control">
                          <?php if(isset($department->active) && $department->active == '1'): ?>
                          <option value="1" selected>Sim</option>
                          <option value="0">Não</option>
                          <?php elseif(isset($department->active) && $department->active == '0'): ?>
                          <option value="1">Sim</option>
                          <option value="0" selected>Não</option>
                          <?php else: ?>
                          <option value="1">Sim</option>
                          <option value="0" selected>Não</option>
                          <?php endif; ?>
                        </select>
                    	</div>
                    </div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success"><?php echo $__env->yieldContent('btnLabel'); ?></button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>