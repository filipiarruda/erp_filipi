<?php $__env->startSection('title', 'Despesas'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $__env->yieldContent('formTitle'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                	<form method="POST" action="<?php echo $__env->yieldContent('route_name'); ?>">
                	<div class="col-md-6">
                    	<div class="form-group">
                    		<label for="">Nome</label>
                    		<input type="text" class="form-control" name="name" id="name" value="<?php echo e(isset($cost->name) ? $cost->name : ''); ?>">
                        <?php echo e(csrf_field()); ?>

                    	</div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Valor</label>
                        <div class="input-group">
                    <div class="input-group-addon">R$</div>
                    <input type="text" class="form-control money" id="value" name="value" placeholder="0.000,00" value="<?php echo e(isset($cost->value) ? number_format($cost->value, 2, ',', '.') : ''); ?>">
                  </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Tipo</label>
                        <select name="type" id="type" class="form-control">
                          <option value="1" <?php echo e(isset($cost->type) && $cost->type == '1' ? 'selected' : ''); ?>>Fixa</option>
                          <option value="2" <?php echo e(isset($cost->type) && $cost->type == '2' ? 'selected' : ''); ?>>Variável</option>
                          <option value="3" <?php echo e(isset($cost->type) && $cost->type == '3' ? 'selected' : ''); ?>>Salário</option>
                          <option value="4" <?php echo e(isset($cost->type) && $cost->type == '4' ? 'selected' : ''); ?>>Outro</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Data de Vencimento</label>
                        <input type="date" class="form-control" name="due_date" id="due_date" value="<?php echo e(isset($cost->due_date) ? $cost->due_date : ''); ?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Data de Pagamento</label>
                        <input type="date" name="payment_date" class="form-control" id="payment_date" value="<?php echo e(isset($cost->date_payment) ? $cost->date_payment : ''); ?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Empresa</label>
                        <select name="company_id" id="company_id" class="form-control">
                          <option value="">Selecione</option>
                          <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                          <option value="<?php echo e($company->id); ?>" <?php echo e(isset($cost->company_id) && $cost->company_id == $company->id ? 'selected' : ''); ?>><?php echo e($company->name); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </select>
                      </div>
                    </div>
              		<div class="col-md-2 col-md-offset-10">
              			<button class="btn btn-success"><?php echo $__env->yieldContent('btnLabel'); ?></button>
              		</div>
              	</form>
              	</div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>