<?php $__env->startSection('title', 'Lista de funcionários'); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Funcionários</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="dataTable">
                        	<thead>
                        		<th>ID</th>
                        		<th>Nome</th>
                        		<th>Login</th>
                                <th>E-mail</th>
                                <th>Ativo</th>
                                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                           	</thead>
                        	<tbody>
                        		<?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php if($employee->resignation_date <> '0001-01-01'): ?>
                                <tr class="danger">
                                <?php else: ?>
                                <tr>
                                <?php endif; ?>
									<td><?php echo e($employee->id); ?></td>
									<td><?php echo e($employee->name); ?></td>
									<td><?php echo e($employee->login); ?></td>
									<td><?php echo e($employee->email); ?></td>
                                    <td>
                                        <?php if($employee->resignation_date <> '0001-01-01'): ?>
                                        Desligado da Empresa
                                        <?php else: ?>
                                        Ativo
                                        <?php endif; ?>
                                    </td>
									<td class="text-center"><a href="<?php echo e(route('form.editEmployee', $employee->id)); ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                </tr>
                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>