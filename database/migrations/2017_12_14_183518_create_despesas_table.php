<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('value', 5, 2);
            $table->integer('type');
            $table->date('due_date');
            $table->date('date_payment');
            $table->boolean('paid_out')->default(false);
            $table->integer('user_id');
            $table->date('register_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_costs');
    }
}
