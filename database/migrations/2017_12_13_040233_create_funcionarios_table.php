<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('office_id');
            $table->string('rg');
            $table->string('cpf');
            $table->string('photo');
            $table->string('login');
            $table->string('password');
            $table->integer('user_id');
            $table->integer('department');
            $table->integer('company_id');
            $table->date('admission_date');
            $table->date('resignation_date');
            $table->date('birth_date');
            $table->date('date_register');
            $table->decimal('salary', 5, 2);
            $table->decimal('percent', 3, 2);
            $table->string('street');
            $table->string('city');
            $table->string('townhouse');
            $table->string('federative_unit');
            $table->string('telephone');
            $table->string('email');
            $table->integer('payment_date');
            $table->string('permissions');
            $table->string('cep');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emloyees');
    }
}
