<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToReceivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('to_receives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('contract');
            $table->decimal('value', 5, 2);
            $table->date('received_date');
            $table->date('due_date');
            $table->boolean('received')->default(false);
            $table->integer('payment_type');
            $table->integer('user_id');
            $table->date('date_register');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_receives');
    }
}
